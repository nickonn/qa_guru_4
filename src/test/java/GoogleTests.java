import com.codeborne.selenide.Browser;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.selenide.AllureSelenide;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selectors.byName;
import static com.codeborne.selenide.Selenide.*;

public class GoogleTests {
//        protected WebDriver driver;

    @BeforeEach
    public void setup() {
        SelenideLogger.addListener("Allure", new AllureSelenide());
//        Configuration.remote = "http://192.168.0.113:4444/wd/hub";
        Configuration.browser = System.getProperty("browser","chrome");
        Configuration.baseUrl = System.getProperty("baseUrl","https://www.google.com/");
//        Configuration.browserVersion = "";
//        DesiredCapabilities capabilities = new DesiredCapabilities();
//        capabilities.setCapability("enableVNC", true);
//        capabilities.setCapability("enableVideo", false);
//        Configuration.browserCapabilities = capabilities;
    }

    @Test
    public void selenideSearchTest() {
        // Открыть google
        open("");

        // Ввести Selenide в поиск
        $(byName("q")).setValue("Selenide").pressEnter();

        // Проверить, что Selenide появился в результатах поиска
        $("#search").shouldHave(text("selenide.org"));

        sleep(8000);
    }

}